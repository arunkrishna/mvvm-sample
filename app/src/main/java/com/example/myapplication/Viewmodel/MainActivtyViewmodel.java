package com.example.myapplication.Viewmodel;

import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.remote.UserRepository;

public class MainActivtyViewmodel extends ViewModel {

    public MutableLiveData<String> data;
    public UserRepository repository;
    public ObservableField<String> nameval = new ObservableField<>("");

    public MainActivtyViewmodel() {
        repository = new UserRepository();
        nameval.set("this second sample");
        data = repository.getResponse();
    }

    public MutableLiveData<String> getData() {
       if (data==null){
            data=new MutableLiveData<>();
        }
        return data;
    }
}
