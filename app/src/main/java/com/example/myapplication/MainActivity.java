package com.example.myapplication;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.myapplication.Viewmodel.MainActivtyViewmodel;
import com.example.myapplication.databinding.ActivityMainBinding;
import com.example.myapplication.presenter.Presenter;

public class MainActivity extends AppCompatActivity {
private ActivityMainBinding binding;
private MainActivtyViewmodel model;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding=DataBindingUtil.setContentView(this,R.layout.activity_main);

        model=ViewModelProviders.of(this).get(MainActivtyViewmodel.class);

        // setContentView(R.layout.activity_main);
        binding.setViewmodel(model);

        model.getData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.myText.setText(s);
            }
        });

        binding.setPresenter(new Presenter() {
            @Override
            public void getData() {
                //first
               // model.data.setValue("first sample");


                //second
                model.nameval.set("update");
            }
        });
    }
}
