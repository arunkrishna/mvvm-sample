package com.example.myapplication.model;

import androidx.lifecycle.MutableLiveData;

public class Welcome {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
