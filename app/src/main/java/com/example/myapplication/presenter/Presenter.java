package com.example.myapplication.presenter;

public interface Presenter {
    void getData();
}
